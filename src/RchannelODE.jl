"""
This solves the steady state R-channel equations for a given discharge
at the snout and fixed input rate.

This follows Werder 2016, see supplement.
"""
module RchannelODE
export Phys, Topo, Num, run_model, zb, zs, thick

using Parameters
using ForwardDiff
using DASSL
using NLsolve

const verbose = false

# Physical parameters
@with_kw immutable Phys @deftype Float64
    day = 24*3600. # secs in day
    L = 3.34e5  # lat. heat of fusion
    g = 9.81 # grav. accel.
    cw = 4220.  # heat cap.

    n = 3. # Glen's n
    A = 2.5e-25 # ice flow constant
    rw = 1000.  # dens water
    ri = 900.   # dens ice
    ct = 7.4e-8 # pressure melt coefficient

    alpha = 5/4 # water flow exponent
    k = 0.05    # channel conductivity
    # fixed beta = 3/2

    gamma  = ct*cw*rw  # pressure melt constant
    kappa = (gamma-1)/(gamma-1+rw/ri)

    # to allow solutions with negative effective pressure:
    include_radius_in_N::Bool = false # 1 or 0
    phi_f = 0.0 #0.755

    # melt input
    m = 0 #0.02/24/3600*7e3  # melt input TODO: set to zero
    Q0 = -100 # discharge at snout: should be negative!

    # Flotation fraction at terminus (used as IC)
    f_factor = 0
end

#######
# Numerical para
@with_kw immutable Num
    Nmin = 0
    verbose = false # print stats after each step
    solver = [:dassl, :oderk][1]
    # oderk
    oderk_steps::Int = 1000
    # dassl
    reltol = 1e-9
    abstol = 1e-8
    calc_IC = false # calculate consistent IC for gradphi
end

###########
# Topography
#
# the following functions need the get a method:

"""
Extend this type to hold the Topo parameters.

Assumes it has a field x which holds the x-coordinates.
"""
abstract Topo

"""
Bed elevation function to be extended for a particular setup.

Needs signature zb{T<:Number}(x::T, t::Topo).
"""
function zb end

"""
Ice thickness function to be extended for a particular setup.

Needs signature thick{T<:Number}(x::T, t::Topo).
"""
function thick end

"""
The topography should be smooth.  This function can be used for that.

Sigmoid function, used for smooth transitions from 0 to 1.

- the maximum slope is 1/(4*width).
- the value decays to 0.01 at x-x0==-4.6*width

1./(1+exp(-(x-x0)./width))
"""
sigmoid(x, x0, width) = 1.0./(1.0+exp(-(x-x0)./width))


zs(x::Number, t::Topo) = zb(x,t)+thick(x,t)
gradzb(x::Number, t::Topo) = derivative(x->zb(x,t), x)
gradthick(x::Number, t::Topo) = derivative(x->thick(x,t), x)

###########
# Relations

# helper
area(R)   = pi*R.^2
radius(S) = sqrt(S./pi)
pressure(h) = rw*g*h
head(p)   = p./rw/g


p_i(x, p::Phys, t::Topo) = p.ri * p.g * thick(x, t)
phim(x, p::Phys, t::Topo) = p.rw * p.g * zb(x, t)
gradphim(x, p::Phys, t::Topo) = derivative(x -> phim(x,p,t), x)
phi0(x, p::Phys, t::Topo) = phim(x, p,t) + p_i(x, p,t)
gradphi0(x, p::Phys, t::Topo) = derivative(x -> phi0(x,p,t), x)
p_w(x,  phi, p::Phys, t::Topo) = phi - phim(x, p, t)

Q(x, p::Phys) = -p.m*x + p.Q0 # problems if Q<0!

N(phi, phi0) = phi0 - phi
Nrad(phi, phi0, S) = phi0 - phi + radius(S)*rw*g  # taking channel filling level into account
Xi(gradphi, Q) = -Q*gradphi
Pi(gradphi, gradphim, Q, p::Phys) = -p.gamma*Q*(gradphi-gradphim)
# in terms of S instead of Q:
XiS(x,S, p::Phys) = abs(Q(x,p)).^3./(p.k^2.*S.*abs(S).^(2*p.alpha-1)) # the abs(S) is to avoid DomainErrors
PiS(x,S, p::Phys, t::Topo) = -p.gamma*(-XiS(x,S,p) - Q(x,p).*gradphim(x, p, t))

"""
This is Equation S11 for dphi/ds=0

Note this has only a physical solution if N>0.
"""
function S(phi, phi0, gradphi, gradphim, Q, p::Phys)
    @unpack p: ri, L, A, n
    N_ = N(phi, phi0)
    if N_<=0
        N_=0
        verbose && println("N<=0")
    end
    s = (Xi(gradphi, Q) - Pi(gradphi, gradphim, Q, p)) / (ri*L*A*abs(N_).^(n-1).* N_)
    if s<0 # this seems to work for the non-linear solver
        s=0
        verbose && println("S<0")
    end
    return s
end

"Eq. S10: dphi/ds = -|Q|Q/(k² S^2α)"
function gradphi_eq(x, phi, S, p::Phys, t::Topo)
    q = Q(x, p)
    - q*abs(q)/( p.k^2 * S^(2*p.alpha) )
end

"Runs the model for given parameters p::Phys, t::Topo"
function run_model(p::Phys, t::Topo, num=Num())
    phistart = phi0(0.0, p, t)*p.f_factor
    if num.solver==:dassl
        xs, phis, gradphis = run_model_dassl(p,t,num,phistart)
    elseif num.solver==:oderk
        xs, phis, gradphis = run_model_oderk(p,t,num,phistart)
    end
    Ss = eltype(xs)[S(ph, phi0(x, p,t), gp, gradphim(x,p,t), Q(x,p), p) for (ph,gp,x) in zip(phis,gradphis,xs)]
    Qs = Q(xs,p)
    return xs, phis, gradphis, Ss, Qs
end

## Implicit ODE solver DASSL.jl
"""
Residual of Eq. S10:
res(dphi/ds,S) = dphi/ds + |Q|Q/(k² S^2α)
"""
function residual(x, phi_, gradphi_, p::Phys, t::Topo)
    # Passed in by DASSL as 1-element vector:
    phi = phi_[1]
    gradphi = gradphi_[1]

    gpm = gradphim(x, p,t)
    q = Q(x, p)
    s = S(phi, phi0(x, p,t), gradphi, gpm, q, p)
    out = [q*abs(q) + p.k^2 * s^(2*p.alpha) * gradphi]
    out
end
function run_model_dassl(p::Phys, t::Topo, num, phistart)
    res(x,phi,dphi) = residual(x, phi, dphi, p, t)
    # consistent IC for phi
    if num.calc_IC
        S_IC = Ssolve(t.x[1] , phistart, p,t)
        gradphi0 = gradphi_eq(t.x[1], phistart, S_IC, p,t)
        if abs(res(t.x[1], phistart, gradphi0))[1]>1e-3
            error("Initial S not calculated well!")
        end
    else
        gradphi0 = 1e6 # better guess high
    end
    # Use iterator version of DASSL solver:
    xs = Float64[t.x[1]]
    phis = Float64[phistart]
    gradphis = Float64[NaN]
    try
        for (x,phi,gradphi) in dasslIterator(res, [phistart], t.x[1];
                                             dy0=[gradphi0], tstop=t.x[end],
                                             reltol=num.reltol, abstol=num.abstol)
            if num.verbose
                @show x,phi[1],gradphi[1]
            end
            push!(xs,x)
            push!(phis,phi[1])
            push!(gradphis, gradphi[1])
            if x>=t.x[end]
                break
            elseif N(phi[1], phi0(x, p, t))<=num.Nmin
                println("N small: $(N(phi[1], phi0(x, p, t)))")
                break
            end
        end
    catch e
        println("Error thrown: $e")
    end
#    xs, phis, gradphis = dasslSolve(res, phistart, x; dy0=[gradphi0])
    return xs, phis, gradphis
end

## Explicit ODE solver
function dphi(x, phi, p::Phys, t::Topo)
    S = Ssolve(x, phi, p,t)
    -Q(x,p)*abs(Q(x,p)) /(p.k^2*abs(S)^(2*p.alpha-1)*S)
end

function run_model_oderk(p::Phys, t::Topo, num, phistart)
    dphifn(x,phi) = dphi(x,phi,p,t)
    reset_Ssolve()
    xout, phis = ode4mod(dphifn, phistart, t.x, p, t, num)
    gradphis = eltype(xout)[dphifn(x,phi) for (x,phi) in zip(xout, phis)]
    return xout, phis, gradphis
end



######
# Calculate S for a give phi.  Can be used to make IC consistent but
# should not be necessary.

# calculate zero of resS to get S (constraint S>=0)
function resS(S, phi, x, p::Phys, t::Topo)
    @unpack p: ri, L, A, n
    out = zeros(1)
    NN = N(phi, phi0(x, p,t))
    out[:] = (XiS(x,S,p)-PiS(x,S, p,t))/(ri*L) - A*S*abs(NN)^(n-1)*NN
    # try
    #     NN = N(phi, phi0(x, p,t))
    #     out[:] = (XiS(x,S,p)-PiS(x,S, p,t))/(ri*L) - A*S*abs(NN)^(n-1)*NN
    # catch e
    #     @show "err", S,phi,x
    #     throw(e)
    # end
    # if isnan(out[1])
    #     @show out[1], S,phi,x
    # end
    return out[1]
end

"Calculates S for given phi."
function Ssolve end
function reset_Ssolve end
let S0=0.1 # use guess for S from last calculation
    function Ssolve(x, phi, p::Phys, t::Topo)
        S0_fact = 5.0
        @unpack p: gamma, k, ri, L, A, n, alpha
        if !p.include_radius_in_N && (gamma==0 || gradphim(x, p,t)==0)
            # analytic
            NN = N(phi, phi0(x, p,t))
            if NN<0
                println("N<0.  Returning last good S")
                return S0*S0_fact
            else
                return ( abs(Q(x,p))^3*(1-gamma) / (k^2*ri*L*A*abs(NN)^(n-1).*NN)  )^(1/(2*alpha+1))
            end
        else
            resS!(S,res) = (res[:] = resS(S[1], phi, x, p, t))
            out = nothing
            try
                out = nlsolve(resS!, [S0], ftol=1e-15) #, autodiff=true) #, ftol=1e-7, method = :newton) #, autodiff = true)
            catch e
                #                @show e
                println("NLsolve errored: $e")
                return S0*S0_fact # return last S0
            end
            if converged(out)
                S0 = out.zero[1]/S0_fact # needs to be lower than solution for sure!
                return out.zero[1]
            else
                res = resS(out.zero[1], phi, x, p, t)
                warn("Nonlinear solve for S not converged. x = $x, Value = $(out.zero), residual = $res")
                return out.zero[1]
            end
        end
    end
    reset_Ssolve() = S0=0.1
end

################
# ODE solvers adapted from ODE.jl v0.1: terminate if N<0
#
# ODE4 Solve non-stiff differential equations, fourth order
# fixed-step Runge-Kutta method.
#
# [T,X] = ODE4(ODEFUN, X0, TSPAN) with TSPAN = [T0:H:TFINAL]
# integrates the system of differential equations x' = f(t,x) from time
# T0 to TFINAL in steps of H with initial conditions X0. Function
# ODEFUN(T,X) must return a column vector corresponding to f(t,x). Each
# row in the solution array X corresponds to a time returned in the
# column vector T.

function ode4mod(F, x0, tspan, p::Phys, t::Topo, num::Num)
    h = diff(tspan)
    x = Array(typeof(x0), length(tspan))
    x[1] = x0
    midxdot = Array(typeof(x0), 4)
    for i = 1:length(tspan)-1
        # Compute midstep derivatives
        midxdot[1] = F(tspan[i], x[i])
        midxdot[2] = 2*F(tspan[i]+h[i]./2, x[i] + midxdot[1].*h[i]./2)
        midxdot[3] = 2*F(tspan[i]+h[i]./2, x[i] + midxdot[2].*h[i]./2)
        midxdot[4] = F(tspan[i]+h[i], x[i] + midxdot[3].*h[i])
        # Integrate
        x[i+1] = x[i] + 1/6 .*h[i].*sum(midxdot)
        # stop if N is negative
        if N(x[i+1], phi0(tspan[i+1], p,t))<0
            #N(tspan[i+1], x[i+1], S(x[i+1], tspan[i+1], depth, width, sslope), depth, width, sslope)<0 # maybe use S(x[i], ...) as S(x[i+1], ...) may fail?
            tspan = tspan[1:i+1]
            x = x[1:i+1]
            if num.verbose
                println("Reached N==0 at x=$(tspan[end]/1e3) km, stopping")
            end
            break
        end
    end
    return tspan, x
end

end # module
