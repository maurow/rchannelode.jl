"""
Defines:

- Topography for simple overdeepening, consisting only of the adverse
  slope.
- Functions to calculated the supercooling threshold
"""
module SimpleOverdeepenings
export ODTopo

using Parameters
using RchannelODE
sigmoid = RchannelODE.sigmoid

#####
# Topography

@enum ODslope linear_slope cos_slope # The different types of slopes
# Topography and related
@with_kw immutable ODTopo <: RchannelODE.Topo  @deftype Float64
    # numerical domain
    domain_len = 30e3
    nn = 100 # grid points
    x::Vector{Float64} = collect(linspace(0, domain_len, nn))

    # surface with linear slope
    surf_off = 400.
    surf_slope = 200/50e3

    # overdeepening (OD)
    od_type::ODslope = [linear_slope, cos_slope][2] # linear slope doesn't work with DASSL
    od_depth = -200.  # dB, negative for down
    od_start = 0  # start of adverse slope, i.e. where it starts to descend
    od_length = 10e3 # length of adverse slope
    transw = 50 # smoothing transition width between different parts of adverse slope
end

# Topo functions
function RchannelODE.zb{T<:Number}(x::T, t::ODTopo)
    @unpack t: od_type, od_start, od_length, od_depth, transw
    od_end = od_start+od_length
    if od_type==linear_slope
        mid =  (x-od_start)*od_depth/od_length
    elseif od_type==cos_slope
        mid = (1-cos( pi*(x-od_start)/od_length ))*od_depth/2
    end
    if transw>0
        return ( (1-sigmoid(x, od_start, transw))*zero(x)
                 + (sigmoid(x, od_start, transw) * (1-sigmoid(x, od_end, transw)) ) * mid
                 +  sigmoid(x, od_end, transw)*(convert(T, od_depth))
                 )
    elseif transw<0 # only transition at bottom of OD
        if x<od_start
            return zero(x)
        else
            return sigmoid(x, od_end, transw)*mid + (1-sigmoid(x, od_end, transw))* convert(T, od_depth)
        end
    else # no smooth transitions
        if x<od_start
            return zero(x)
        elseif x>od_end
            return convert(T, od_depth)
        else
            return mid
        end
    end
end

RchannelODE.thick(x::Number, t::ODTopo) = t.surf_off + x*t.surf_slope - RchannelODE.zb(x,t)

#####
# Analytic threshold formulas

corr_fact(p::Phys, t::ODTopo) = thick(0, t)*(1-p.f_factor)
classic_thresh_dB(dS, kappa) = kappa*dS
classic_thresh_dB(p::Phys, t::ODTopo) = p.kappa*(zs(t.od_start+t.od_length,t) - zs(t.od_start,t))
new_thresh_dB(dS, h1, f, kappa) = kappa*(dS + h1*(1-f))
function new_thresh_dB(p::Phys, t::ODTopo)
    h1 = thick(t.od_start,t)
    dS = zs(t.od_start+t.od_length,t) - zs(t.od_start,t)
    p.kappa*(dS + h1*(1-p.f_factor))
end


## ODE model

"""
Calculate supercooling threshold depth ΔB for a given f and ΔS.
"""
function calc_thresh_dB(p::Phys, t::ODTopo, start=10.0; tol=1.0, num=Num())
    if start>=0
        start = min(0.0, classic_thresh_dB(p,t)+10)
    end
    fun = dB->apprfn(dB,p,t,num)
    dB_tol = 1.0
    dB_step = 20.0
    return approach(fun, start, t.od_start+t.od_length, dB_step, dB_tol)
end

"Used as objective function in approach"
function apprfn(dB,p,t,num)
    t = ODTopo(t, od_depth=dB)
    xs, phis, gradphis, Ss, Qs = run_model(p, t, num)
    t, xs[end]
end

"""
Approach threshold depth from shallower depth.  Stop at depth when integration
does not reach the end of the domain anymore.
"""
function approach(fun, start_dB, xend, step, tol)
    maxiter = 100
    dB = start_dB
    t, x = fun(dB)
    println("")
    println("   $dB, $x")
    if !(x>=xend)
        @show dB, x
        error("Bad initial dB: should be chosen such that N=0 is NOT reached in overdeepening")
    end
    for i=1:maxiter
        dB -= step
        t, x = fun(dB)
        println("   $dB, $x")
        if x>=xend # reached end, i.e. N>0
            #            step *= 1.5
            nothing
        else # N<0
            if step<=tol # stop if reached tolerance
                return t, dB
            end
            # else redo with smaller step
            dB += step
            step /= 2
        end
    end
    warn("Maxiter exceeded in approach-fn.")
    return t, dB
end
end
