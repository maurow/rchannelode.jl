# This script plots figure 2 from the Werder 2016 paper.
using JLD
import RchannelODE
R = RchannelODE
push!(LOAD_PATH, ".")
import SimpleOverdeepenings
OD = SimpleOverdeepenings
using PyPlot
using OrgTables # https://github.com/mauro3/OrgTables.jl

#######################
## Figure 2: model runs
#######################
calc_yes = true # set to true to do simulations
plot_yes = true  # set to true to do plotting
load_data = false # load stored data instead of calculating it
if calc_yes
    # Default parameters. Change here to change them all:
    pp = R.Phys()
    tt = OD.ODTopo(od_type=SimpleOverdeepenings.cos_slope,
                   #od_type=SimpleOverdeepenings.linear_slope,
                   transw=0,
                   domain_len=41e3, # needs to be larger than adverse slope
                   od_start=0,
                   nn=1000)
    dS = 40.0
    #num = R.Num()#calc_IC=true, verbose=true, abstol=1e-6, reltol=1e-7)
    num = R.Num(solver=:oderk, oderk_steps = 1000)

    dxs = [0.5, 2, 5, 10, 20, 40]*1e3 # length of adverse slope
    #dxs = [.5]*1e3 # length of adverse slope
    fs = collect(reverse(linspace(0,1,21))) # different f to calculate with
    fs[1] = 0.999 # 1.0 is not liked so much.
    dBs = zeros(length(dxs), length(fs)) # threshold dB

    t,p =1,1
    @time for (i,dx) in enumerate(dxs)
        xs = vcat(collect(0:49),linspace(50.0, dx+1e3, tt.nn))
        t = OD.ODTopo(tt, od_length=dx, domain_len=dx+1e3, x=xs,
                      surf_slope = dS/dx)
        num = R.Num(num, oderk_steps = floor((dx+1e3)/1.))
        dB_start = 0.0
        for (j,f) in enumerate(fs)
            print("$dx, $f, ")
            p = R.Phys(pp, f_factor=f)
            t, dBs[i,j] = OD.calc_thresh_dB(p, t, dB_start; num=num)
            println("$(dBs[i,j])")
            dB_start = dBs[i,j] + 5 # set new starting value
        end
    end

    @save "data/fig2-data-cos.jld" tt pp dxs fs dBs dS
end

if load_data
    @load "data/fig2-data-cos.jld"
end

###
# Data from GlaDS runs
mf   = 1:-0.1:0.1  # flotation factors
mdB2  = -[80, 170, 200, 260, 320, 370, 420, 500, 570, 590] # depth at N=0 for Δx=2km
mdB20 = -[80, 120, 160, 200, 200, 209, 215, 240, 245, 250] # depth at N=0 for Δx=20km

# plot it
if plot_yes
    # setup
    h1 = tt.surf_off
    corr_fact = h1*(1-fs)
    m_corr_fact = h1*(1-mf)

    typ = 2
    xvar = Vector{Float64}[fs, corr_fact][typ]
    classic = Float64[OD.classic_thresh_dB(dS,pp.kappa) for f in fs]
    new  = Float64[OD.new_thresh_dB(dS,h1,f,pp.kappa) for f in fs]

    # attrs
    lw1 = 1.5
    lw2 = 3

    matplotlib[:rc]("font", family="normal", size=13) #, weight="bold",
    pygui(false)

    figure(figsize=(5.0, 5.0))
    ax1 = subplot(111)
    subplots_adjust(left=0.164, right=0.95)
    hold(true)
    xleg = Any[] # legend strings
    # plot classic
    plot(xvar, classic, linewidth=lw2, "r")
    push!(xleg, "classic")
    col = ["y", "c", "m", "g", "purple", "pink"]
    for (i,dx) in reverse(collect(enumerate(dxs)))
        rdx = floor(Int,dx/1e3)
        plot(xvar, dBs[i,:]', linewidth=lw1, color=col[i])
        push!(xleg, latexstring("\$\\Delta x=$rdx\$ km"))
        if dx==2e3
            plot(m_corr_fact, mdB2, "x", markeredgecolor=col[i], markeredgewidth=lw1)
            push!(xleg, latexstring("\$\\Delta x=$rdx\$ km 2D"))
        elseif dx==20e3
            plot(m_corr_fact, mdB20, "x", markeredgecolor=col[i], markeredgewidth=lw1)
            push!(xleg, latexstring("\$\\Delta x=$rdx\$ km 2D"))
        end
    end
    xleg[end] = latexstring("\$\\Delta x=0.5\$ km")
    # plot analytic
    plot(xvar, new, linewidth=lw2, "k")
    push!(xleg, "new")
    legend(xleg, loc="lower left", frameon=false, fontsize=12)
    # plot again to put on top
    i=2
    plot(m_corr_fact, mdB2, "x", markeredgecolor=col[i], markeredgewidth=lw1)
    i=5
    plot(m_corr_fact, mdB20, "x", markeredgecolor=col[i], markeredgewidth=lw1)

    ylabel(L"$\Delta B \,$ (m)")
    if typ==1
        xlabel(L"f")
        ax2 = ax1[:twiny]()
        xlim([0, corr_fact[end]])
        xlabel(L"$h_1(1-f)\,$ (m)")
    else
        xlabel(L"$h_1(1-f)\,$ (m)")
        ax2 = ax1[:twiny]()
        xlim([1,0])
        xlabel(L"f")
    end
    ylim([-800,10])

    savefig("data/fig2.pdf")


    ## plot difference cosine and linear slope

    lin = load("data/fig2-data-lin.jld")

    figure(figsize=(5.0, 5.0))
    ax1 = subplot(111)
    subplots_adjust(left=0.164, right=0.95)
    hold(true)
    xleg = Any[] # legend strings

    col = ["y", "c", "m", "g", "purple", "pink"]
    for (i,dx) in reverse(collect(enumerate(dxs)))
        rdx = floor(Int,dx/1e3)
        plot(xvar, dBs[i,:]'-lin["dBs"][i,:]', linewidth=lw1, color=col[i])
        push!(xleg, latexstring("\$\\Delta x=$rdx\$ km"))
    end
    xleg[end] = latexstring("\$\\Delta x=0.5\$ km")
    legend(xleg, loc="upper left", frameon=false, fontsize=12)

    ylabel(L"$\Delta B_{\mathrm{sine}}- \Delta B_{\mathrm{linear}}\,$ (m)")
    xlabel(L"$h_1(1-f)\,$ (m)")
    ax2 = ax1[:twiny]()
    xlim([1,0])
    xlabel(L"f")
    ylim([0,26])
    savefig("data/fig2-lin-cos.pdf")
end
