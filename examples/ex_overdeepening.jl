# Just runs & plots the model.

import RchannelODE
R = RchannelODE
include("SimpleOverdeepenings.jl")
OD = SimpleOverdeepenings

# use mostly default para:
p = R.Phys(f_factor=.35)
t = OD.ODTopo(od_length=.5e3,
              od_start=0,
              od_depth=-10,
              transw=-40
              )
#num = R.Num(calc_IC=true, verbose=true)
num = R.Num(solver=:oderk)
#plot(t.x, [R.zb(x, t) for x in t.x])

# run the model
@time xs, phis, gradphis, Ss, Qs = R.run_model(p,t,num);
@show xs[end], phis[end]

# plot it
using PyPlot

ax=subplot(2,1,1)
plot(xs, phis/1e6)
hold(true)
plot(xs, [R.phi0(x, p, t) for x in xs]/1e6)
plot(xs, [R.phim(x, p, t) for x in xs]/1e6)
ylabel(L"\phi \quad (MPa)")
subplot(2,1,2,sharex=ax)
plot(xs, Ss)
plot(xs, -Qs)
ylabel(L"S (m), Q (m^3/s)")
ylim([0,120])
