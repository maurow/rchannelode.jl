# Example using an inclined, parallel sided slab glacier

import RchannelODE
R = RchannelODE
push!(LOAD_PATH, ".")
using PyPlot
using Parameters

@with_kw immutable Inclined <: RchannelODE.Topo  @deftype Float64
    # numerical domain
    domain_len = 30e3
    nn = 100 # grid points
    x::Vector{Float64} = collect(linspace(0, domain_len, nn))

    # surface and bed with linear slope
    thickness = 400.
    slope = 200/domain_len
end

# Define the two needed topography functions:
RchannelODE.zb{T<:Number}(x::T, t::Inclined) = x*t.slope
RchannelODE.thick(x::Number, t::Inclined) = t.thickness*one(x)

# Model parameters
pa = R.Phys()  # physical parameters, all defautls
topo = Inclined() # topography, all defautls
num = R.Num(solver=:oderk) # numerics, using R-K solver

# Run the model
@time xs, phis, gradphis, Ss, Qs = R.run_model(pa,topo,num);
@show xs[end], phis[end]

# Make a plot
ax=subplot(2,1,1)
plot(xs, phis/1e6)
hold(true)
plot(xs, [R.phi0(x, pa, topo) for x in xs]/1e6)
plot(xs, [R.phim(x, pa, topo) for x in xs]/1e6)
ylabel(L"\phi \quad (MPa)")
subplot(2,1,2,sharex=ax)
plot(xs, Ss)
plot(xs, -Qs)
ylabel(L"S (m), Q (m^3/s)")
ylim([0,120])
