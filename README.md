# RchannelODE

This is implements a simple steady state R-channel model.  This was used in [Werder 2016](http://onlinelibrary.wiley.com/doi/10.1002/2015GL067542/full) ([preprint](https://maurow.bitbucket.io/docs/Werder_2016.pdf), [supplement](https://maurow.bitbucket.io/docs/Werder_2016-supp.pdf)).

Examples are in `examples/`
