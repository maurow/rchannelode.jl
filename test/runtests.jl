using RchannelODE
R = RchannelODE
using Parameters
using Base.Test


### Test against analytic solution of Rothlisberger 1972
### for a box glacier
#####################
function phi_ana(x, phi0, pp, topo)
    @unpack pp: alpha, A, L, ri, gamma, n, Q0, k

    pi = R.p_i(0, pp, topo)
    N0 = pi - phi0

    delta = 2*alpha*n /(1+2*alpha)
    kappa = ( abs(Q0)^(2-2*alpha)/k^2 * (A*ri*L/(1-gamma))^(2*alpha) )^(1/(2*alpha+1))
    N = ( -(1-delta)*kappa*x + N0^(1-delta)).^(1/(1-delta))
    return pi - N
end


### RchannelODE setup
## Box glacier
@with_kw immutable Box <: RchannelODE.Topo  @deftype Float64
    # numerical domain
    domain_len = 200e3
    nn = 10^4 # grid points
    x::Vector{Float64} = collect(linspace(0, domain_len, nn))

    # surface and bed with linear slope
    thickness = 400.
end
# Define the two needed topography functions:
RchannelODE.zb{T<:Number}(x::T, t::Box) = x*0
RchannelODE.thick(x::Number, t::Box) = t.thickness*one(x)

# Model parameters
pa = R.Phys(f_factor=1e-3)  # physical parameters, all defautls
topo = Box() # topography, all defautls
num = R.Num(solver=:oderk) # numerics, using R-K solver

# Run the model
xs, phis, gradphis, Ss, Qs = R.run_model(pa, topo, num);

# calculate analytic solution
phia = phi_ana(xs, phis[1], pa, topo)
@test maximum(abs(phis-phia)./phia)< 0.005

# using PyPlot
# subplot(2,1,1)
# plot(xs, phis/1e6)
# hold(true)
# plot(xs, phia/1e6)
# plot(xs, map(x->R.p_i(x, pa, topo), xs)/1e6)
# subplot(2,1,2)
# plot(xs,abs(phis-phia)./phia)
